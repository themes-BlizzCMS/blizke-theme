# BLizke Theme

* Theme for BlizzCMS-Plus.

[![Project Version](https://img.shields.io/badge/Version-1.0-green.svg?style=flat-square)](#)


https://wow-cms.com/en/ - https://github.com/WoW-CMS/BlizzCMS

# Project

* https://wow-cms.com/en/
* https://github.com/WoW-CMS/BlizzCMS

# Installation

* Add the "Blizke" folder in /application/themes/
* Then go to the admin panel and rename the theme to "Blizke" save changes and you're good to go.

# Requirements

**_BlizzCMS Plus_**

## Copyright

Copyright © 2021 [WoW-CMS](https://wow-cms.com) - 2021 [Tyrael#4918]

## Warning

* The credits of this theme cannot be removed by the user, in that case it will no longer receive support from the developers.

* This theme cannot be distributed or shared unless you download it directly from this site. In this case, support will be discontinued and future updates will not have access to the repository.


# Screenshots

![Screenshot](index.png)
![Screenshot](nav.png)
![Screenshot](store.png)
![Screenshot](forum.png)
![Screenshot](panel.png)